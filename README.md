## maze_gen - A maze image generator

### Summary

maze_gen is a program that is built for the purpose of testing image recognition.  It it built with cmake, vala, gdk-pixbuf-2.0, and GSL, so it will run on most every system that downloads those packages.  At some point, this may be expanded so that this is able to rotate the generation perspective.

### Features

 * Set cell size - The maze assumes that all cells are made of the same size.  This size can be set, and greatly affects the size of the resultant maze image.
 * Set number of nodes - This program can create a varying number of nodes in the x and y direction.
 * Set maze edge buffer - Set the amount of buffer space around the maze.  Useful when the maze to be viewed is a subset of the region.
 * Set wall thickness - Change the thickness of the wall.  This can make detection easier or harder.
 * Random image salting - Salt some percentage of the maze with uniform random data.
 * Random Wall Data - Set a mu value for the exponential random distribution for the wall data.  The default data for the wall is 255, while the floor is 0. 
