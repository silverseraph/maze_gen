public class PriorityQueue<T>
{
    private int _count;
    private int size;
    private _Entry<T>[] data;

    public int count { get { return this._count; } }

    private class _Entry<T>
    {
        public double priority;
        public T data;
        //public int i_ref_count = 1;

        public _Entry(double priority, T item) { this.priority = priority; this.data = item; }
    }

    public const int INITIAL_SIZE = 1024;

    public PriorityQueue()
    {
        this.size = INITIAL_SIZE;
        this._count = 0;
        this.data = new _Entry<T>[this.size];
    }

    public void clear()
    {
        for(int i = 0; i < this._count; i++) this.data[i] = null;
    }

    public void offer(double priority, T item)
    {
        assert(item != null);

        if(this.size == this._count)
        {
            //stderr.printf("PQ: Resizing up from %d to %d\n", this.size, this.size << 1);
            this.size <<= 1; 
            this.data.resize(this.size);
            if(this.data == null)
            {
                stderr.printf("Failed to resize heap. Exiting...\n");
                Process.exit(-1);
            }
        }

        this.data[this._count] = new _Entry<T>(priority, item);

        this.bubbleup(this._count);

        this._count++;
    }

    public T poll(out double? priority = null)
    {
        if(this._count == 0)
        {
            priority = double.MAX;
            return null;
        }

        /*
        if((this._count < (this.size << 2)) && this._count > INITIAL_SIZE)
        {
            stderr.printf("PQ: Resizing down from %d to %d\n", this.size, this.size >> 1);
            this.size >>= 1;
            this.data.resize(this.size);
        }
        */

        //store off relevant data, then kill the struct
        _Entry<T> tmp = this.data[0];
        T ret = tmp.data;
        priority = tmp.priority;
        //move tail element to head
        this.data[0] = this.data[this._count-1];
        this._count--;
        this.trickledown(0);
        return ret;
    }

    public void print_raw()
    {
        for(int i = 0; i < this._count; i++) stdout.printf("%5.3f ", this.data[i].priority);
        stdout.putc('\n');
    }

    private inline int parent(int child)
    {
        if(child != 0)
        {
            return (child -1) >> 1;
        }
        else
        {
            return -1;
        }
    }

    private inline bool idx_valid(int idx) { return ((idx > -1) && (idx < this._count)); }

    private inline int left(int parent)
    {
        int idx = (parent << 1) + 1;
        if(idx_valid(idx)) return idx;
        else return -1;
    }

    private inline int right(int parent)
    {
        int idx = (parent << 1) + 2;
        if(idx_valid(idx)) return idx;
        else return -1;
    }

    private void bubbleup(int idx)
    {
        while((idx > 0) && (parent(idx) >= 0) && (this.data[parent(idx)].priority > this.data[idx].priority))
        {
            _Entry<T> data = this.data[parent(idx)];
            this.data[parent(idx)] = this.data[idx];
            this.data[idx] = data;
            idx = parent(idx);
        }
    }

    private void trickledown(int idx)
    {
        int left_idx = left(idx);
        if(!idx_valid(left_idx)) return;

        int smaller_idx = left_idx;

        int right_idx = right(idx);

        if(idx_valid(right_idx))
        {
            if(this.data[right_idx].priority < this.data[left_idx].priority) 
            {
                smaller_idx = right_idx;
            }
        }

        if(this.data[smaller_idx].priority < this.data[idx].priority)
        {
            _Entry<T> data = this.data[idx];
            this.data[idx] = this.data[smaller_idx];
            this.data[smaller_idx] = data;
            trickledown(smaller_idx);
        }
    }
}
