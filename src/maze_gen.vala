[CCode(cname="exit")]
private extern void exit(int exit_code);

int main(string[] args)
{
    Params parms = new Params(args);

    Grid g = new Grid(parms.nodes[0], parms.nodes[1]);

#if DEBUG
    g.print();
#endif

    g.run_mst(0, 0, parms.remove);

#if DEBUG
    g.print();
#endif

    Image img = new Image(g, parms);

    img.fill();

    img.write();

    return 0;
}
