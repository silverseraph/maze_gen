public class Grid
{
    public long dim[2];

    public Vertex[,] vertices;

    public Grid(long x, long y)
    {
        this.dim[0] = x;
        this.dim[1] = y;

        assert(x > 0);
        assert(y > 0);

        this.vertices = new Vertex[this.dim[0], this.dim[1]];

        //create vertices
        for(long i = 0; i < this.dim[0]; i++)
        {
            for(long j = 0; j < this.dim[1]; j++)
            {
                this.vertices[i,j] = new Vertex(this.dim[0], this.dim[1]);
            }
        }

        //create links
        for(long i = 0; i < this.dim[0]; i++)
        {
            for(long j = 0; j < this.dim[1]; j++)
            {
                if(i+1 < this.dim[0]) //add left
                {
#if DEBUG
                    stdout.printf("linking [%ld,%ld] and [%ld,%ld]\n", i, j, i+1, j);
#endif
                    this.add_edge_horiz(this.vertices[i, j], this.vertices[i+1, j]);
                }
                if(j+1 < this.dim[1]) //add up
                {
#if DEBUG
                    stdout.printf("linking [%ld,%ld] and [%ld,%ld]\n", i, j, i, j+1);
#endif
                    this.add_edge_vert(this.vertices[i, j], this.vertices[i, j+1]);
                }
            }
        }
    }

    private void add_edge_vert(Vertex a, Vertex b)
    {
#if DEBUG
        stdout.printf("Adding vertical edge between %p and %p\n", a, b);
#endif
        Edge e = new Edge(a, b);

        if(a.down == null || b.up == null)
        {
            a.down = e;
            b.up = e;
        }
    }

    private void add_edge_horiz(Vertex a, Vertex b)
    {
#if DEBUG
        stdout.printf("Adding horizontal edge between %p and %p\n", a, b);
#endif
        Edge e = new Edge(a, b);

        if(a.down == null || b.up == null)
        {
            a.right = e;
            b.left = e;
        }
    }

    public void run_mst(long seed_x_idx = 0, long seed_y_idx = 0, long num_remove = 0)
    {
        assert(seed_x_idx < this.dim[0]);
        assert(seed_y_idx < this.dim[1]);

        PriorityQueue<Edge> pq = new PriorityQueue<Edge>();
        
        Edge e = null;
        Vertex a = null;
        Vertex b = null;

        a = this.vertices[seed_x_idx, seed_y_idx];
        a.in_mst = true;
        a.add_to_pq(pq);

        //while(pq.count > 0)
        while(null != (e = pq.poll()))
        {
            log_info("Poll'd data: %p\n", e);
            a = e.a;
            b = e.b;

            //if both edges are in the mst, continue
            if(a.in_mst && b.in_mst)
            {
                if(num_remove > 0)
                {
                    e.in_mst = true;
                    num_remove--;
                }
                else
                {
                    continue;
                }
            }

            //if a isn't in the mst and b is, add a to the mst, add all edges on a to the mst
            if(!a.in_mst && b.in_mst)
            {
                log_info("Added vertex[%ld,%ld,%p] to the mst\n", a.idx_x, a.idx_y, a);
                e.in_mst = true; //set edge to be in mst
                a.in_mst = true; //set a as in mst
                a.add_to_pq(pq);
            }

            if(a.in_mst && !b.in_mst)
            {
                log_info("Added vertex[%ld,%ld,%p] to the mst\n", b.idx_x, b.idx_y, b);
                e.in_mst = true;
                b.in_mst = true;
                b.add_to_pq(pq);
            }   
        }
    }

    public void print()
    {
        stdout.printf("GRID:\n");
        for(long j = 0; j < this.dim[1]; j++)
        {
            for(long i = 0; i < this.dim[0]; i++)
            {
                Vertex v = this.vertices[i, j];
                stdout.printf("Vertex[%ld, %ld, %p]:\n", i, j, v);
                if(v.up != null)
                    stdout.printf("\tup   [%3s, %p]\n", v.up.in_mst ? "in" : "out", v.up);
                else
                    stdout.printf("\tup   [ (DNE) ]\n");
                if(v.down != null)
                    stdout.printf("\tdown [%3s, %p]\n", v.down.in_mst ? "in" : "out", v.down);
                else
                    stdout.printf("\tdown [ (DNE) ]\n");
                if(v.left != null)
                    stdout.printf("\tleft [%3s, %p]\n", v.left.in_mst ? "in" : "out", v.left);
                else
                    stdout.printf("\tleft [ (DNE) ]\n");
                if(v.right != null)
                    stdout.printf("\tright[%3s, %p]\n", v.right.in_mst ? "in" : "out", v.right);
                else
                    stdout.printf("\tright[ (DNE) ]\n");
            }
        }
    }
}
