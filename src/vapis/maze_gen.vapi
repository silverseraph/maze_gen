[PrintfFormat]
[CCode(cheader_filename="maze_gen.h", cname="log_err")]
public void log_err(string fmt, ...);

[PrintfFormat]
[CCode(cheader_filename="maze_gen.h", cname="log_warn")]
public void log_warn(string fmt, ...);

[PrintfFormat]
[CCode(cheader_filename="maze_gen.h", cname="log_info")]
public void log_info(string fmt, ...);

[CCode(cheader_filename="maze_gen.h", cname="mg_unused", simple_generics=true)]
public void unused<T>(T unused);
