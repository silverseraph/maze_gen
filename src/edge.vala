public class Edge
{
    public unowned Vertex a;
    public unowned Vertex b;

    public bool in_mst;

    public double weight;

    public Edge(Vertex a, Vertex b)
    {
        this.a = a;
        this.b = b;
        this.in_mst = false;
        this.weight = Random.next_double();
    }
}
