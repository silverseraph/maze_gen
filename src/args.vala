public class Params
{
    public long cell[2];
    public long nodes[2];
    public long buff[2];
    public float salt;
    public float mu = 4;

    public long wall;
    public long remove;

    public string output;

    public Params(string[] args)
    {
        for(long d = 0; d < 2; d++)
        {
            this.cell[d] = 10;
            this.nodes[d] = 10;
            this.buff[d] = 0;
        }
        
        this.wall = 1;
        this.remove = 0;
        this.salt = 0;

        this.output = "output.png";

        long temp[2];

        if(args.length < 2)
        {
            show_help(args[0]);
            exit(-1);
        }

        for(int i = 1; i < args.length; i++)
        {
            switch(args[i])
            {
                case "-h":
                case "--help":
                    show_help(args[0]);
                    exit(-1);
                    break;
                case "-c":
                case "--cell":
                    i++;
                    if(i < args.length)
                    {
                        if(2 == args[i].scanf("%d,%d", &temp[0], &temp[1]))
                        {
                            this.cell[0] = temp[0];
                            this.cell[1] = temp[1];
                        }
                        else
                        {
                            log_err("Failed to read two integers for cell size on cell argument.\n");
                        }   
                    }
                    else
                    {
                        log_warn("No argument was provided to cell argument.\n");
                    }
                    break;
                case "-n":
                case "--nodes":
                    i++;
                    if(i < args.length)
                    {
                        if(2 == args[i].scanf("%d,%d", &temp[0], &temp[1]))
                        {
                            this.nodes[0] = temp[0];
                            this.nodes[1] = temp[1];
                        }
                        else
                        {
                            log_err("Failed to match two integrs for node argument.\n");
                        }
                    }
                    else
                    {
                        log_warn("No argument was provided to nodes argument.\n");
                    }
                    break;
                case "-b":
                case "--buffer":
                    i++;
                    if(i < args.length)
                    {
                        if(2 == args[i].scanf("%d,%d", &temp[0], &temp[1]))
                        {
                            this.buff[0] = temp[0];
                            this.buff[1] = temp[1];
                        }
                        else
                        {
                            log_err("Failed to match two integers to buffer argument.\n");
                        }
                    }
                    else
                    {
                        log_warn("No argument was provided to buffer argument.\n");
                    }
                    break;
                case "-w":
                case "--wall":
                    i++;
                    if(i < args.length)
                    {
                        if(1 == args[i].scanf("%d", &temp[0]))
                        {
                            this.wall = temp[0];
                        }
                        else
                        {
                            log_err("Failed to read an integer for the wall argument.\n");
                        }
                    }
                    else
                    {
                        log_warn("No argument provided to wall argument.\n");
                    }
                    break;
                case "-r":
                case "--remove":
                    i++;
                    if(i < args.length)
                    {
                        if(1 == args[i].scanf("%d", &temp[0]))
                        {
                            if(temp[0] < 0)
                            {
                                log_warn("The number of removed edges must be positive. Defaulting to 0.\n");
                                temp[0] = 0;
                            }
                            this.remove = temp[0];
                        }
                    }
                    else
                    {
                        log_warn("No argument provided to remove argument.\n");
                    }
                    break;
                case "-o":
                case "--output":
                    i++;
                    if(i < args.length)
                    {
                        this.output = args[i];
                    }
                    else
                    {
                        log_warn("No argument provided to output argument.\n");
                    }
                    break;
                case "-s":
                case "--salt":
                    i++;
                    if(i < args.length)
                    {
                        if(1 == args[i].scanf("%f", &this.salt))
                        {
                            if(this.salt < 0 || this.salt > 1)
                            {
                                this.salt = 0;
                                log_err("The option to the salt argument must be on the range [0,1].  Defaulting to 0...\n");
                            }
                        }
                        else
                        {
                            log_err("Failed to read a float on the salt argument.\n");
                        }
                    }
                    else
                    {
                        log_warn("No argument provided to salt argument.\n");
                    }
                    break;
                case "-m":
                case "--mu":
                    i++;
                    if(i < args.length)
                    {
                        if(1 == args[i].scanf("%f", &this.mu))
                        {
                            if(this.mu < 0 || this.mu> 255)
                            {
                                log_err("The entered value for mu was \"%g\", but the value must be on the range of [0,255].  Defaulting to %g\n", this.mu, 4);
                                this.mu = 4;
                            }
                        }
                        else
                        {
                            log_err("Failed to read a float on the mu argument.\n");
                        }
                    }
                    else
                    {
                        log_warn("No argument provided to mu argument for random distribution.\n");
                    }
                    break;
                default:
                    log_warn("args[%d] = \"%s\" does not fit the input argument.\n", i, args[i]);
                    break;
            }
        }
    }

    public void show_help(string name)
    {
        stdout.printf("Usage: %s \n", name);
        stdout.printf("     -h, --help          Show the help, then exit.\n");
        stdout.printf("     -c, --cell [x,y]    Set the size of each cell as x,y.\n");
        stdout.printf("     -n, --nodes [x,y]   Set the number of nodes as x,y.\n");
        stdout.printf("     -b, --buffer [x,y]  Set the buffer pixels as x,y\n");
        stdout.printf("     -w, --wall [w]      Set the size of the walls on each size of the cell.\n");
        stdout.printf("     -s  --salt [p]      Set the salt percentage.\n");
        stdout.printf("     -m  --mu [v]        Set a floating point value on the range [0,255] for the walls.\n");
        stdout.printf("     -r, --remove        Set the number of nodes to remove after the MST runs.\n");
        stdout.printf("     -o, --output [file] Set the file output location.\n");
    }
}
