using Gdk;
using Gsl;

public class Image
{
    public long dim[2];
    public float[,] data;
    private Grid g;
    private Params p;

    public Image(Grid g, Params p)
    {
        this.g = g;
        this.p = p;

        this.dim[0] = (p.cell[0] * p.nodes[0]);
        this.dim[1] = (p.cell[1] * p.nodes[1]);

        this.data = new float[this.dim[0], this.dim[1]];
    }

    public void fill()
    {
        long ww = this.p.wall; //wall width
        long cdim[2];
        long grid_idx[2]; //idx in the grid
        long cell_idx[2]; //idx within the cell
        Vertex cell = null, cu = null, cd = null, cl = null, cr = null;
        cdim[0] = this.p.cell[0];
        cdim[1] = this.p.cell[1];

        for(long j = 0; j < this.dim[1]; j++)
        {
            grid_idx[1] = j / cdim[1];
            cell_idx[1] = j % cdim[1];
            for(long i = 0; i < this.dim[0]; i++)
            {
                grid_idx[0] = i / cdim[0];
                cell_idx[0] = i % cdim[0];
                this.data[i,j] = 0f;

                cell = this.g.vertices[grid_idx[0], grid_idx[1]];

                if(cell.up != null)
                    cu = cell.up.a;
                else
                    cu = null;
                if(cell.down != null)
                    cd = cell.down.b;
                else
                    cd = null;
                if(cell.left != null)
                    cl = cell.left.a;
                else
                    cl = null;
                if(cell.right != null)
                    cr = cell.right.b;
                else
                    cr = null;

                //if it's an outer wall, add it unconditionally
                if(i < ww || i >= (dim[0] - ww)) this.data[i,j] = 1f;
                if(j < ww || j >= (dim[1] - ww)) this.data[i,j] = 1f;

                //if it's in a wall space for the left wall
                if(cell_idx[0] < ww)
                    if(cell.left != null)
                        if(!cell.left.in_mst)
                            this.data[i,j] = 1f;

                if(cell_idx[0] >= (cdim[0] - ww))
                    if(cell.right != null)
                        if(!cell.right.in_mst)
                            this.data[i,j] = 1f;

                if(cell_idx[1] < ww)
                    if(cell.up != null)
                        if(!cell.up.in_mst)
                            this.data[i,j] = 1f;

                if(cell_idx[1] >= (cdim[1] - ww))
                    if(cell.down != null)
                        if(!cell.down.in_mst)
                            this.data[i,j] = 1f;

                //upper left
                if(cell_idx[0] < ww && cell_idx[1] < ww)
                {
                    if(cu != null)
                        if(cu.left != null)
                            if(!cu.left.in_mst)
                                this.data[i,j] = 1f;
                    if(cl != null)
                        if(cl.up != null)
                            if(!cl.up.in_mst)
                                this.data[i,j] = 1f;
                }

                //lower left
                if(cell_idx[0] < ww && cell_idx[1] >= (cdim[1] - ww))
                {
                    if(cl != null)
                        if(cl.down != null)
                            if(!cl.down.in_mst)
                                this.data[i,j] = 1f;
                    if(cd != null)
                        if(cd.left != null)
                            if(!cd.left.in_mst)
                                this.data[i,j] = 1f;
                }

                //upper right
                if(cell_idx[0] >= (cdim[0] - ww) && cell_idx[1] < ww)
                {
                    if(cr != null)
                        if(cr.up != null)
                            if(!cr.up.in_mst)
                                this.data[i,j] = 1f;
                    if(cu != null)
                        if(cu.right != null)
                            if(!cu.right.in_mst)
                                this.data[i,j] = 1f;
                }

                //lower right
                if(cell_idx[0] >= (cdim[0] - ww) && cell_idx[1] >= (cdim[1] - ww))
                {
                    if(cr != null)
                        if(cr.down != null)
                            if(!cr.down.in_mst)
                                this.data[i,j] = 1f;
                    if(cd != null)
                        if(cd.right != null)
                            if(!cd.right.in_mst)
                                this.data[i,j] = 1f;
                }
            }
        }
    }

    public void write()
    {
        Pixbuf buff = new Pixbuf(
            Colorspace.RGB, 
            false,
            8,
            (int)((2 * this.p.buff[0]) + this.dim[0]),
            (int)((2 * this.p.buff[1]) + this.dim[1]));

#if DEBUG
        stdout.printf("Image metadata:\n");
        stdout.printf("     bps:        %d\n", buff.bits_per_sample);
        stdout.printf("     has_alpha:  %s\n", buff.has_alpha ? "true" : "false");
        stdout.printf("     height:     %d\n", buff.height);
        stdout.printf("     width:      %d\n", buff.width);
        stdout.printf("     n_channels: %d\n", buff.n_channels);
        stdout.printf("     rowstride:  %d\n", buff.rowstride);
#endif

        int rs = buff.rowstride;
        int dx = buff.width;
        int dy = buff.height;
        int nc = buff.n_channels;
        int os = 0;
        uint8 pv = 0;
        float pvf = 0f;

        uint8 *pdata = (uint8*)buff.pixels;

        Gsl.RNG r = new Gsl.RNG(RNGTypes.rand48);

        TimeVal t = TimeVal();
        t.get_current_time();
        r.@set((ulong)(t.tv_sec * 1000 + t.tv_usec));

        for(int j = 0; j < dy; j++) //for each row in the image
        {
            os = rs * j;
            if(j < this.p.buff[1] || (dy - this.p.buff[1] - 1) < j)
            {
                for(int i = 0; i < dx; i++) //for each column in the row
                {
                    for(int k = 0; k < nc; k++) pdata[os+k] = 0; //set channels to 0
                    os += nc;
                }
            }
            else
            {
                for(int i = 0; i < dx; i++) //for each column in the row
                {
                    //in the buff region.  set it to 0
                    if(i < this.p.buff[0] || (dx - this.p.buff[0] - 1) < i)
                    {
                        for(int k = 0; k < nc; k++) pdata[os+k] = 0; //set channels to 0
                    }
                    else //in the actual image.  Center as img[img_x, img_y] = grid[img_x - buff[0], img_y - buff[1]]
                    {
                        pvf = Math.fabsf((float)((this.data[i-this.p.buff[0], j-this.p.buff[1]] + Gsl.Randist.gaussian(r, p.mu/255)) * 255));
                        if(pvf < 0) pvf = 0;
                        if(pvf > 255) pvf = 255;
                        pv = (uint8)pvf;
                        for(int k = 0; k < nc; k++) pdata[os+k] = pv;
                    }
                    os += nc;
                }
            }
        }

        long n_salt_pix = (long)Math.floorf((float)(buff.width * buff.height) * p.salt);

        for(long i = 0; i < n_salt_pix; i++)
        {
            int x = Random.int_range(0, buff.width);
            int y = Random.int_range(0, buff.height);
            uint8 v = (uint8)Random.int_range(0, 255);

            int idx = (y * buff.rowstride) + (x * nc);

            for(int j = 0; j < nc; j++) pdata[idx+j] = v;
        }

        string fn = this.p.output;

        string[] tokens = fn.split(".", 2);

        string ft = tokens[1];

        try
        {
            buff.save(fn, ft);
        }
        catch(GLib.Error e)
        {
            log_err("%s\n", e.message);
        }
    }
}
