public class Vertex
{
    public long idx_x;
    public long idx_y;

    public bool in_mst;

    public Edge up;
    public Edge down;
    public Edge left;
    public Edge right;

    public Vertex(long idx_x, long idx_y)
    {
        this.idx_x = idx_x;
        this.idx_y = idx_y;

        this.in_mst = false;

        this.up = null;
        this.down = null;
        this.left = null;
        this.right = null;
    }

    public void reset()
    {
        this.in_mst = false;
        if(this.up != null) this.up.in_mst = false;
        if(this.down != null) this.down.in_mst = false;
        if(this.left != null) this.left.in_mst = false;
        if(this.right != null) this.right.in_mst = false;
    }

    public void add_to_pq(PriorityQueue<Edge> pq)
    {
        if(this.up != null) 
            if(!this.up.a.in_mst || !this.up.b.in_mst)
                pq.offer(this.up.weight, this.up);
        if(this.down != null)             
            if(!this.down.a.in_mst || !this.down.b.in_mst)
                pq.offer(this.down.weight, this.down);
        if(this.left != null) 
            if(!this.left.a.in_mst || !this.left.b.in_mst)
                pq.offer(this.left.weight, this.left);
        if(this.right != null) 
            if(!this.right.a.in_mst || !this.right.b.in_mst)
                pq.offer(this.right.weight, this.right);
    }
}
