#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <glib.h>

#ifndef __maze_gen_utils__
#define __maze_gen_utils__

#ifndef NO_COLOR
#define MAGENTA "\033[1;31m"
#define ORANGE  "\033[1;33m"
#define GREEN   "\033[1;32m"
#define BLUE    "\033[1;34m"
#define PURPLE  "\033[1;35m"
#define WHITE   "\033[1;37m"
#define RESET   "\033[m"
#else
#define MAGENTA ""
#define ORANGE  ""
#define GREEN   ""
#define BLUE    ""
#define PURPLE  ""
#define WHITE   ""
#define RESET   ""
#endif

#define ERROR "[ERROR]: "
#define WARN  "[WARN]:  "
#define INFO  "[INFO]:  "
#define NIFTY "[NIFTY]: "

//always show errors.  your face is about to melt off
#define log_err(M, ...)   fprintf(stderr, MAGENTA ERROR "(%s:%d:%s) " M RESET, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__)

//always show warnings.  these are bad, but not terrible
#define log_warn(M, ...)  fprintf(stderr, ORANGE  WARN  "(%s:%d:%s) " M RESET, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__)

//show interesting events always, use sparingly
#define log_nifty(M, ...) fprintf(stdout, PURPLE  NIFTY "(%s) "       M RESET,                     __FUNCTION__, ##__VA_ARGS__)

//show debug events, go crazy with this
#ifdef DEBUG
#define log_info(M, ...)  fprintf(stdout, GREEN   INFO  "(%s:%d:%s) " M RESET, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__)
#else
#define log_info(M, ...)
#endif

#define mg_unused(var) (void)(var)

#endif
